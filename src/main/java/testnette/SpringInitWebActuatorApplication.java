package testnette;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringInitWebActuatorApplication
{
	public static void main(String[] args)
	{
		SpringApplication.run(SpringInitWebActuatorApplication.class, args);
	}
}
